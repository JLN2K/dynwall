#!/bin/bash

## !! START OF USER CONFIGURATION !!
wallFolder="/home/example/wallpaper"
## !! END OF USER CONFIGURATION !!

## Function to change wallpaper - Usage: changeWall [00-23]
function changeWall {
    newimghs=$(sha1sum $wallFolder/$1.png | awk '{ print $1 }')
    oldimghs=$(sha1sum $wallFolder/wallpaper.png | awk '{ print $1 }')
    if [ $newimghs != $oldimghs ]
    then
        cp $wallFolder/$1.png $wallFolder/wallpaper.png
    fi
}

## Main loop
while true;
do
    hour=$(date +%H)
    changeWall $hour
    sleep 900
done
