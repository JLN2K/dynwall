# Dynwall
Dynwall is a simple script that changes your wallpaper throughout the day based on the time. I made this really quickly for personal use, it's not designed to be super user-friendly or flexible.
## Supported environments
This has been tested on Gnome 3, but should run in any desktop environment which constantly refreshes the wallpaper from the file. (ex. when you set *wallpaper.png* as wallpaper and replace the file with another, the wallpaper directly updates to the new file's contents).
## Usage
**Setting up wallpaper files** 

1. Create a folder somewhere in your home folder where your wallpapers will be stored.
2. Copy the images (have to be .png) to the folder, and name them 00.png to 23.png based on the time of day (ex. 14.png is 2PM).
3. Duplicate/Copy one of the images to *wallpaper.png* in the same folder.
4. Set *wallpaper.png* as your desktop wallpaper. (Gnome: `gsettings set org.gnome.desktop.background picture-uri file:///home/EXAMPLE/wallpaper/wallpaper.png`)

**Setting up the script**

5. Download the script to somewhere on your system.
6. Open the script in your favorite text editor and edit the 4th line: `wallFolder="/home/example/wallpaper"` to the path where you set up your images earlier. (so if you stored your images in a folder called "wallpaper" in your home dir, and your username is *dave*, change this line to `wallFolder="/home/dave/wallpaper"`)
7. Now you can run the script! You can either run it in the background or setup a service in systemd to run it automatically.